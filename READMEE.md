# mvc-example
Dengan konsep MVC ini, website seakan memiliki bagian yang terpisah dan bisa dikembangkan masing-masing. Maka, proses pembuatan website bisa dilakukan lebih cepat karena developer akan lebih fokus pada pengerjaan salah satu bagian saja. 
Dalam contoh ini tidak ada routing controller melalui address bar, melainkan menggunakan query browser biasa.